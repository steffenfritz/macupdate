#!/bin/zsh
if ! command -v mas &> /dev/null
then
    echo "mas could not be found. Install it with brew install mas."
    exit
fi
brew update
brew upgrade
brew cleanup -s
brew doctor
brew missing
brew autoremove
mas outdated
mas upgrade
